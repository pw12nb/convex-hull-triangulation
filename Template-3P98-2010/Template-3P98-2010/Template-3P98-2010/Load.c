/* An example of how to read an image (img.tif) from file using freeimage and then
display that image using openGL's drawPixelCommand. Also allow the image to be saved
to backup.tif with freeimage and a simple thresholding filter to be applied to the image.
Conversion by Lee Rozema.
Added triangle draw routine, fixed memory leak and improved performance by Robert Flack (2008)
*/

#include <stdlib.h>
#include <math.h>


#ifdef __unix__
	#include <GL/freeglut.h>
#elif defined(_WIN32) || defined(WIN32)
	#include <freeglut.h>
#endif


#include <FreeImage.h>
#include <stdio.h>
#include <malloc.h>


//the pixel structure
typedef struct {
	GLubyte r, g, b;
} pixel;


//random point linked list
struct node{
    int x, y, drawn;
	struct node *next;
};
typedef struct node node;

//random point linked list
struct edge{
    node *start, *end;
	struct edge *next;
};
typedef struct edge edge;

//the global structure
typedef struct {
	pixel *data;
	int w, h, pointCount, numClusters;
	struct node *headCluster;
	struct node *tailCluster;
	struct node *tailNode;
	struct node *headNode;
	struct edge *tailEdge;
	struct edge *headEdge;
} glob;
glob global;


void AddCluster(node *add)
{
	struct node *newPoint; 
	newPoint = (node*)(malloc( sizeof(struct node) ));  
	newPoint->next = 0;
	newPoint->x = add->x;
	newPoint->y = add->y;
	newPoint->drawn = 0;

	if(!global.headCluster)
	{
		global.headCluster = newPoint;
		global.tailCluster = global.headCluster;
		global.numClusters++;
	}
	else
	{
		global.tailCluster->next = newPoint; 
		global.tailCluster = newPoint;
		global.numClusters++;
	}
}

void RemoveCluster(node *clust)
{
	struct node *current = global.headCluster;

	//if its the head
	if(current->x == clust->x &&
				current->y == clust->y)
	{
		global.headCluster = global.headCluster->next;
		global.numClusters--;
		free(current);
		return;
	}

	if(clust)
	{
		while(current)
		{
			if(current->next->x == clust->x &&
				current->next->y == clust->y)
			{
				global.numClusters--;
				current->next = current->next->next;
				free(clust);
				return;
			}
			current = current-> next;
		}
	}
}

void AddPoint(int x, int y)
{
	struct node *newPoint; 
	newPoint = (node*)(malloc( sizeof(struct node) ));  
	newPoint->next = 0;
	newPoint->x = x;
	newPoint->y = y;
	newPoint->drawn = 0;

	if(!global.headNode)
	{
		global.headNode = newPoint;
		global.tailNode = global.headNode;
		global.pointCount++;
	}
	else
	{
		global.tailNode->next = newPoint; 
		global.tailNode = newPoint;
		global.pointCount++;
	}
}

node* RemovePoint(node *dest)
{
	struct node *current = global.headNode;

	//if its the head
	if(current->x == dest->x &&
				current->y == dest->y)
	{
		global.headEdge = global.headEdge->next;
		global.pointCount--;
		free(current);
		return global.headEdge->next;
	}

	if(dest)
	{
		while(current)
		{
			if(current->next->x == dest->x &&
				current->next->y == dest->y)
			{
				global.pointCount--;
				current->next = current->next->next;
				free(dest);
				return current->next;
			}
			current = current-> next;
		}
	}
	else
		return NULL;

	if(!dest) return NULL;
}

edge* AddEdge(node *start, node* end)
{
	struct edge *newEdge; 
	newEdge = (edge*)(malloc( sizeof(struct edge) ));  
	newEdge->next = 0;
	newEdge->start = start;
	newEdge->end = end;

	if(!global.headEdge)
	{
		global.headEdge = newEdge;
		global.tailEdge = global.headEdge;
	}
	else
	{
		global.tailEdge->next = newEdge; 
		global.tailEdge = newEdge;
	}

	return newEdge;
}

void RemoveEdge(edge *e)
{
	edge* current = global.headEdge;
	//if its the head
	if(current->start == e->start &&
				current->end == e->end)
	{
		global.headEdge = global.headEdge->next;
		free(current);
		return;
	}
	//else iterate to find it
	if(e)
	{
		while(current)
		{
			if(current->next->start == e->start &&
				current->next->end == e->end)
			{
				current->next = current->next->next;
				free(e);
				return;
			}
			current = current-> next;
		}
	}
}

int InCluster(node *point, node*mid)
{
	node* current = global.headCluster;
	double currentDistance = pow(((double)(point->x - mid->x)),2)+pow(((double)(point->y - mid->y)),2);

	while(current)
	{
		if(currentDistance > pow(((double)(point->x - current->x)),2)+pow(((double)(point->y - current->y)),2))
			return -1;
		current = current->next;
	}

	return 1;
}

node* selectNPoint(int n)
{
	node* current = global.headNode;
	int i;
	for(i = 0; i < n; i++)
		current = current->next;

	return current;
}


void GenerateNPoints(int n)
{
	int i;
	for(i = 0; i < n; i++)
	{
		AddPoint((rand() % global.w), (rand() % global.h));
	}
}

void GridOfPoints(int n)
{
	int i, j;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			AddPoint(i*(global.w/n), j*(global.h/n));
		}
	}
}

void CopyPoints()
{
	node* points = global.headNode;
	int x, y;
	
	glColor3f(1,1,1); 
    glPointSize(1.0);
	glBegin(GL_POINTS);
	
	while(points)
	{
		glVertex2f(points->x , points->y);
		points = points->next;
	}
	
	glEnd();

	glFlush();
}

void initScreen()
{
	int x,y;
	global.w = 1000; 
	global.h = 800;
	global.pointCount = 0;
	global.numClusters = 0;
	global.data = (pixel *)malloc((global.h)*(global.w)*sizeof(pixel));
	for(x = 0; x < global.w; x++)
	{
		for(y = 0; y < global.h; y++)
		{
			global.data[(x)+(y*global.w)].r = 0;
			global.data[(x)+((y)*global.w)].g = 0;
			global.data[(x)+((y)*global.w)].b = 0;
		}
	}
}

//read image
pixel *read_img(char *name, int *width, int *height) {
	FIBITMAP *image;
	int i,j,pnum;
	RGBQUAD aPixel;
	pixel *data;

	if((image = FreeImage_Load(FIF_TIFF, name, 0)) == NULL) {
		return NULL;
	}      
	*width = FreeImage_GetWidth(image);
	*height = FreeImage_GetHeight(image);

	//data = (pixel *)malloc((*height)*(*width)*sizeof(pixel *));
	data = (pixel *)malloc((*height)*(*width)*sizeof(pixel));
	pnum=0;
	for(i = 0 ; i < (*height) ; i++) {
		for(j = 0 ; j < (*width) ; j++) {
			FreeImage_GetPixelColor(image, j, i, &aPixel);
			data[pnum].r = (aPixel.rgbRed);
			data[pnum].g = (aPixel.rgbGreen);
			data[pnum++].b = (aPixel.rgbBlue);
		}
	}
	FreeImage_Unload(image);
	return data;
}//read_img

//write_img
void write_img(char *name, pixel *data, int width, int height) {
	FIBITMAP *image;
	RGBQUAD aPixel;
	int i,j;

	image = FreeImage_Allocate(width, height, 24, 0, 0, 0);
	if(!image) {
		perror("FreeImage_Allocate");
		return;
	}
	for(i = 0 ; i < height ; i++) {
		for(j = 0 ; j < width ; j++) {
			aPixel.rgbRed = data[i*width+j].r;
			aPixel.rgbGreen = data[i*width+j].g;
			aPixel.rgbBlue = data[i*width+j].b;

			FreeImage_SetPixelColor(image, j, i, &aPixel);
		}
	}
	if(!FreeImage_Save(FIF_TIFF, image, name, 0)) {
		perror("FreeImage_Save");
	}
	FreeImage_Unload(image);
}//write_img


/*draw the image - it is already in the format openGL requires for glDrawPixels*/
void display_image(void)
{
	glDrawPixels(global.w, global.h, GL_RGB, GL_UNSIGNED_BYTE, (GLubyte*)global.data);	
	glFlush();
}//display_image()

int PointsEqual(node* point1, node* point2)
{
	if (point1->x == point2->x && point1->y == point2->y){
		return 1;
	}
	return 0;
}

int Min(int a, int b)
{
	if (a < b)
		return a;
	else
		return b;
}

int Max(int a, int b)
{
	if (a > b)
		return a;
	else
		return b;
}


void Quick_Hull(node* head, edge* line)
{
	node* point = head;
	node* clusters;
	node* max = NULL;
	int maxDistance = -1;
	int d, minD, current;
	while(point)
	{
		//if the point is inbetween the line and is not drawn yet
		if (point->drawn == 0 && !PointsEqual(line->start, point) && !PointsEqual(line->end, point))
		{
			d = Distance(line, point);

			if (d == 0)
			{
				//check if collinear point
				//if slope is infinity, use the points to determine if it is in between
				if (line->start->y - point->y == 0 && (Min(line->start->x, line->end->x) > point->x || Max(line->start->x, line->end->x) < point->x))
					continue; 
				//if slope is 0, use the points
				else if (line->start->x - point->x == 0 && (Min(line->start->y, line->end->y) > point->y || Max(line->start->y, line->end->y) < point->y))
					continue;
				//otherwise, use opposing slopes to determine if in the middle
				else if ((((line->start->x - point->x) / (line->start->y - point->y)) + ((line->end->x - point->x) / (line->end->y - point->y))) != 0)
					continue;

			}
			if(d > maxDistance)
			{
				max = point;
				maxDistance = d;
			}
		}

		point = point->next;
	}

	if(maxDistance >= 0)
	{
		edge* one = AddEdge(line->start, max);
		edge* two = AddEdge(max, line->end);
		RemoveEdge(line);

		Quick_Hull(head, one);
		Quick_Hull(head, two);
	}
	/*
	else if (maxDistance == 0)
	{
		if ()
		{
			edge* one = AddEdge(line->start, max);
			edge* two = AddEdge(max, line->end);
			RemoveEdge(line);

			Quick_Hull(head, one);
			Quick_Hull(head, two);
		}
	}*/
}

node* MinPoint(node *mid)
{
	node* current = mid;
	node* min = NULL;
	while(current)
	{
		if((!min || current->y < min->y) && current->drawn == 0)
			min = current;

		current = current->next;
	}
	return min;
}

node* MaxPoint(node *mid)
{
	node* current = mid;
	node* max = NULL;
	while(current)
	{
		if((!max || current->y > max->y) && current->drawn == 0)
			max = current;

		current = current->next;
	}
	return max;
}

void Draw_Edges()
{
	edge* next;
    glPointSize(4.0);  
	while(global.headEdge)
	{
		glBegin(GL_LINES);	
		glVertex2d(global.headEdge->start->x, global.headEdge->start->y);
		glVertex2d(global.headEdge->end->x, global.headEdge->end->y);
		glEnd();

		//remove it from the next hull
		global.headEdge->start->drawn++;
		global.headEdge->end->drawn++;	

		RemoveEdge(global.headEdge);
	
	}
	glFlush();	
	printf("Point Count: %d\n",global.pointCount);
}


int Convex_Hull(node *i)
{
	while(1 == 1)
	{
		node* min = MinPoint(i);
		node* max = MaxPoint(i);

		if(min && max)
		{
			edge *top = AddEdge(min,max);
			edge *bottom = AddEdge(max,min);
		
			Quick_Hull(i, top);		
			Quick_Hull(i, bottom);
		}

		if(!global.headEdge)
			return;
		Draw_Edges();
	}
}

int Cluster(int n)
{
	node *current;
	node *newPoint;
	node *list = NULL;
	node *points;
	while(global.numClusters < n)
	{
		AddCluster(selectNPoint(rand() % global.pointCount));
	}

	current = global.headCluster;
	while(current)
	{
		points = global.headNode;
		while(points)
		{
			if(InCluster(points,current) == 1)
			{ 
				newPoint = (node*)(malloc( sizeof(struct node) ));  
				newPoint->next = 0;
				newPoint->x = points->x;
				newPoint->y = points->y;
				newPoint->drawn = 0;

				if(!list)
					list = newPoint;
				else
					newPoint->next = list;
					list = newPoint;
			}

			points = points->next;

		}
		glColor3ub(rand() % 256, rand() % 256, rand() % 256);
		Convex_Hull(list);
		while(list)
		{
			points = list;
			list = list->next;
			free(points);
		}
		current = current->next;
	}
}

int Distance(edge* e, node* pointC)
{
	int ABx = e->end->x - e->start->x;
    int ABy = e->end->y - e->start->y;
    int num = ABx * (e->start->y - pointC->y) - ABy * (e->start->x - pointC->x);
    return num;
}




/*glut keyboard function*/
void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 0x1B:
		case'q':
		case 'Q':
			free(global.data);
			exit(0);
			break;
		case's':
		case'S':
			printf("SAVING IMAGE: backup.tif\n");
			write_img("backup.tif", global.data, global.w, global.h);
			break;
		case'r':
		case'R':
			GenerateNPoints(100);
			CopyPoints();
			break;
		case 'c':
		case 'C':
			glColor3f(0.0,0.4,0.1); 
			Convex_Hull(global.headNode);
			break;			
		case 'h':
		case 'H': 
			Cluster(5);
			break;
		case 'g':
		case 'G':
			GridOfPoints(20);
			CopyPoints();
			break;
	}
}//keyboard

void menuItems(int num)
{
	switch(num)
	{
		case 0:
			GenerateNPoints(100);
			CopyPoints();
			break;
		case 1:
			glColor3f(0.0,0.4,0.1); 
			Convex_Hull(global.headNode);
			break;
	}
}




void createMenu()
{ 
	int menu_id = glutCreateMenu(menuItems);
    glutAddMenuEntry("Add 100 random", 0);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

}

void placePointOnClick(int button, int state, int x, int y)
{
	AddPoint(x, (global.h - 15) - y);
	CopyPoints();
}

int main(int argc, char** argv)
{
	initScreen();
	if (global.data==NULL)
	{
		printf("Error loading image file img.tif\n");
		return 1;
	}
	printf("Q:quit\nS:save\nR:Random Point\nC:Convex Hull");
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB|GLUT_SINGLE);
	
	createMenu();
	glutInitWindowSize(global.w,global.h);
	glClearColor (0.0, 0.0, 0.0, 0.0);
	glutCreateWindow("SIMPLE DISPLAY");
	glShadeModel(GL_SMOOTH);
	glutDisplayFunc(display_image);
	glutMouseFunc(placePointOnClick);
	glutKeyboardFunc(keyboard);
	glMatrixMode(GL_PROJECTION);
	glOrtho(0,global.w,0,global.h,0,1);
	
	glutMainLoop();

	return 0;
}
